package VirtualDiskTesters;
import VirtualDiskExceptions.InvalidFileException;

/**
 * The purpose of this class is to test VirtualDiskExceptions behavior 
 * or functionality.
 * @author Ivan J. Miranda Marrero
 *
 */
public class ExceptionTester 
{
	public static void main(String[] args) 
	{
		try
		{
			throw new InvalidFileException("File Specified is not a random access file.");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try
		{
			throw new InvalidFileException();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		/* 
		 * TODO We can test other exceptions whenever we feel to. We can also test them when
		 * whichever methods that throw whichever exceptions are implemented.
		 */
		
		System.out.println("Program didn't terminate in last lines.");
		
	}
	
}
