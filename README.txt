Developed by Ivan J. Miranda.

Welcome. This project is in its development phase. THE IDEA FOR THIS
PROJECT IS NOT MINE. THIS PROJECT IS BEING MADE FOR A DATA STRUCTURES
CLASS. NOW, I DO TAKE CREDIT FOR THE IMPLEMENTATION OF THE IDEA. With
that clear, we can continue.

LAST UPDATE: 27/09/2015 11:05am.

_____________________________________________________________________
SPECIFICATIONS AND MAIN IDEA:

In this programming project, we will work with virtual disks systems, 
and eventually use it in a system that will implement a simple virtual 
file system. In this phase, we will implement the basis of the system, 
which is the virtual disk in which data is stored in blocks. In this 
initial phase of the project, we will implement a component of the 
system that emulates a single disk unit. 

A single disk unit is a simple component described as follows. 
It consists of a sequence of a fixed number of blocks 
(all of the same size - number of bytes) and a set of disk operations. 
A block corresponds to an area (think of space available) consisting 
of a fixed number of bytes. It supports a group of operations that
can be performed by a disk unit.

The storage area for the disk unit to be implemented will reside on 
a system�s random access file. See class RandomAccessFile in Java APIs.

_____________________________________________________________________

The core of the project is already implemented and working as intended
(in what it has been tested) but still some minor features (not
(essential) can be implemented and the writing process to the disk can
be optimized. In the source files you can find some of these features
commented in wherever they can be implemented. The documentation
is complete and you can find some output files whose output came from
the testers.

_____________________________________________________________________

Finally, the random access files are already created for the examples
used. To recreate, delete those files and run tester0. Tester1 outputs
according to the disk specified (see commented lines in tester.)