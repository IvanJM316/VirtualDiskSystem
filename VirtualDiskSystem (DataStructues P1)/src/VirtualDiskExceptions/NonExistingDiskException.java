package VirtualDiskExceptions;
@SuppressWarnings("serial")

/**
 * This exception is thrown when the disk specified in the
 * process being carried is not an existing disk, that is,
 * its random access file doesn't exist.
 * @author Ivan J. Miranda Marrero
 *
 */
public class NonExistingDiskException extends Exception 
{
	/**
	 * Creates a new throwable NonExistingDiskException with 
	 * its default message.
	 */
	public NonExistingDiskException()
	{
		super("Disk specified doesn't exist.");
	}
	
	/**
	 * Creates a new throwable NonExistingDiskException with 
	 * the specified message.
	 * @param message the message to be printed when thrown.
	 */
	public NonExistingDiskException(String message)
	{
		super(message);
	}
}
