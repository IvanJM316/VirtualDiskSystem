package VirtualDiskUnit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidParameterException;

import VirtualDiskExceptions.*;

/**
 * Instances of this class represent a disk unit: A simple component that consists
 * of a sequence of a fixed number of blocks, all of the same size (number of bytes), 
 * and a set of disk operations. A block corresponds to an area consisting of a fixed 
 * number of bytes. It emulates the behavior of a disk storage unit (hard disk) and
 * can be called for abstraction purposes a virtual disk. The storage area for the 
 * disk unit will reside on a system�s random access file.
 * @author Ivan J Miranda Marrero
 *
 */
public class DiskUnit 
{
	private final static int DEFAULT_DISK_CAPACITY = 1024; //Default number of blocks. 	
	private final static int DEFAULT_BLOCK_CAPACITY = 256; //Default number of bytes per block.
	private int diskCapacity; //Number of blocks in current disk instance.
	private int blockCapacity; //Capacity of each block in current disk instance.
	private DiskBlock[] block;
	private RandomAccessFile disk; //File emulating disk storage area.
	
//	/**
//	 * Default constructor. Creates a disk that consists of 1024 blocks of space, 
//	 * each block consisting of 256 bytes of space.
//	 */
//	public DiskUnit()
//	{
//		this(DEFAULT_DISK_CAPACITY, DEFAULT_BLOCK_CAPACITY);
//	}
//	
//	/**
//	 * Creates a disk unit consisting of diskCapacity blocks, each with blockSize 
//	 * bytes of space. Both values need to be positive powers of 2. Otherwise,
//	 * the declared exception is thrown.
//	 * @param diskCapacity the number of blocks the disk will consist of.
//	 * @param blockCapacity the number of bytes each block can hold.
//	 * @throws InvalidParameterException if any of the parameters is not positive
//	 * or not a power of 2.
//	 */
//	public DiskUnit(int diskCapacity, int blockCapacity) throws InvalidParameterException
//	{
//		if (diskCapacity < 0 || blockCapacity < 0 || !powerOf2(diskCapacity) || !powerOf2(blockCapacity)) 
//			throw new InvalidParameterException("Invalid values: " + "diskCapacity = " + diskCapacity 
//												+ " blockCapacity = " + blockCapacity); 
//		//Parameters are valid.
//		
//		try {
//			//For the moment, �DISK� is the name of the file that is
//			//used in you program�s directory where the random access
//			//file is created. This may change in future phases. 
//			
//			disk = new RandomAccessFile("DISK", "rw");
//
//			//Writes (diskCapacity * blockCapacity) bytes to the random access file
//			//filling it with zeroes. 
//		
//			block = new DiskBlock[diskCapacity];
//			for (int i = 0; i < diskCapacity; i++)
//			{
//				block[i] = new DiskBlock(blockCapacity);
//				block[i].writeBlock(disk);
//			}	
//			
//		    } catch (IOException e) {
//		    	System.err.println ("Unable to start the disk.");
//		    	System.exit(1);
//		    }
//		
//		    //We will eventually do something else here, but for the
//		    //moment keep it as it is. 
//		
//		    this.diskCapacity = diskCapacity; 
//		    this.blockCapacity = blockCapacity; 
//		    reserveDiskSpace(disk, diskCapacity, blockCapacity);
//	}
//
//	/**
//	 * Creates a disk unit that will use the specified file as its storage area in
//	 * the computer�s disk. The file is assumed to be initialized as a valid storage 
//	 * area of a DiskUnit, namely, a random access file.
//	 * @param fileName the name or path of the file.
//	 * @throws FileNotFoundException if the specified file or path does not exist.
//	 * @throws InvalidFileException if the file exists but is not a random access 
//	 * file.
//	 */
//	public DiskUnit(String fileName) throws FileNotFoundException, InvalidFileException
//	{ 
//		File file = new File(fileName);
//		if (!file.exists())
//			throw new FileNotFoundException("File does not exist: " + fileName);
//		
//		RandomAccessFile disk = new RandomAccessFile("fileName", "rw");
//	
//		try {
//			disk.seek(0);
//			diskCapacity = disk.readInt();
//			blockCapacity = disk.readInt();
//			disk.seek(0); //We reset so we can read all the data to the virtual blocks.
//			block = new DiskBlock[diskCapacity];
//			for (int i = 0; i < diskCapacity; i++)
//			{
//				block[i] = new DiskBlock(blockCapacity);
//				block[i].readBlock(disk);
//			}	
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//	}
	
	/**
	 * Creates a disk unit whose name is the String specified.
	 * @param name the name of the disk.
	 */
	private DiskUnit(String name)
	{
		try {
			//Creates new random access file with the given name that will
			//emulate disk storage.
			
			disk = new RandomAccessFile(name, "rw");
			
		    } catch (IOException e) {
		    	System.err.println ("Unable to start the disk.");
		    	System.exit(1);
		    }
	}
	
	/**
	 * Creates a new disk unit with the given name. The disk is formatted
	 * as having a default capacity (number of blocks) of 1024, each of 
	 * default bytes capacity (number of bytes) of 256. The created disk is left 
	 * "turned off" or "dismounted".
	 * @param name the name of the random access file that will to represent 
	 * the disk storage area.
	 * @throws ExistingDiskException whenever the name attempted is
	 * already in use.
	 */
	public static void createDiskUnit(String name) throws ExistingDiskException
	{
	    createDiskUnit(name, DEFAULT_DISK_CAPACITY, DEFAULT_BLOCK_CAPACITY);
	}
	   
	/**
	 * Creates a new disk unit with the given name. The disk is formatted
	 * as with the specified capacity (number of blocks), each of specified
	 * bytes capacity (number of bytes). The created disk is left "turned off"
	 * or "unmounted".
	 * @param name the name of the file that is to emulate the disk's storage.
	 * @param diskCapacity the number of blocks in the new disk.
	 * @param blockCapacity the capacity per block in the new disk.
	 * @throws ExistingDiskException whenever the name specified is
	 * already in use.
	 * @throws InvalidParameterException whenever the values for diskCapacity
	 * or blockCapacity are not positive or powers of 2.
	 */
	public static void createDiskUnit(String name, int diskCapacity, int blockCapacity)
	throws ExistingDiskException, InvalidParameterException
	{
		File file = new File(name);
		if (file.exists())
			throw new ExistingDiskException("Disk name is already in use: " + name);
	   	
	    //RandomAccessFile disk = null;
	    
	    if (diskCapacity < 0 || blockCapacity < 0 || !powerOf2(diskCapacity) || !powerOf2(blockCapacity))
	    	throw new InvalidParameterException("Invalid values: " + "disk capacity = " + diskCapacity 
	    										+ " block capacity = " + blockCapacity);
	    
	    //Disk parameters are valid.
	    //We proceed to create the random access file that will emulate the new disk's storage area.
	    
	    DiskUnit dUnit = new DiskUnit(name);
	    
	    try {
	    	
	    	//disk = new RandomAccessFile(name, "rw");
	    	
			dUnit.block = new DiskBlock[diskCapacity];
			for (int i = 0; i < diskCapacity; i++)
			{
				dUnit.block[i] = new DiskBlock(blockCapacity);
				dUnit.block[i].writeBlock(dUnit.disk);
			}
	    	
	    } catch (IOException e) {
	        System.err.println ("Unable to initialize the disk.");
	        System.exit(1);
	    }

	    reserveDiskSpace(dUnit.disk, diskCapacity, blockCapacity);
	   	
	    //After creation, we leave the disk "turned off" or "unmounted" or "shut down"
	    //by just closing the corresponding random access file.
	    try {
	    	dUnit.disk.close();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	}
	
	/**
	 * Writes the content of block b into the disk block corresponding to the block 
	 * number specified by blockNum, overwriting whatever was stored before in such 
	 * disk block, in the current disk instance. 
	 * @param blockNumber the block number in which data will be written on this disk.
	 * @param newBlock the block containing such data.
	 * @throws InvalidBlockNumberException if the block number does not represent a
	 * valid block position, analogous to going out of bounds in an array.
	 * @throws InvalidBlockException if block b's size does not match the block size
	 * of which blocks in this disk instance are made of, or if b is null.
	 */
	public void write(int blockNumber, DiskBlock newBlock) throws InvalidBlockNumberException, InvalidBlockException
	{
		if(blockNumber > diskCapacity - 1)
			throw new InvalidBlockNumberException("Block number specified does not exist: " + blockNumber);
		if(newBlock.getCapacity() != blockCapacity)
			throw new InvalidBlockException("The capacity of this disk blocks does not match the capacity"
											+ " of the block sent: This: " + blockCapacity + " Sent: " + 
											newBlock.getCapacity());
		
		for(int i = 0; i < blockCapacity; i++)
			block[blockNumber].setByte(i, newBlock.getByte(i)); //Assigning newBlock to block will just copy the reference.
		
		//TODO implement a more efficient method of updating the file. Check DiskBlock class instance variables for the idea.
		try {
			disk.seek(0);
			for (DiskBlock b : block)
				b.writeBlock(disk); //Rewrites complete file with updated disk. 
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Reads the specified block from the disk and copies its content to the block 
	 * being referenced by parameter b, overwriting block b's previous data. 
	 * @param blockNumber the block number from which data will be read on this disk.
	 * @param newBlock the block where such data will be written.
	 * @throws InvalidBlockNumberException if the block number does not represent a
	 * valid block position, analogous to going out of bounds in an array.
	 * @throws InvalidBlockException if block b's size does not match the block size
	 * of which blocks in this disk instance are made of, or if b is null.
	 */
	public void read(int blockNumber, DiskBlock newBlock) throws InvalidBlockNumberException, InvalidBlockException
	{
		if(blockNumber > diskCapacity - 1)
			throw new InvalidBlockNumberException("Block number specified does not exist: " + blockNumber);
		if(newBlock.getCapacity() != blockCapacity)
			throw new InvalidBlockException("The capacity of this disk blocks does not match the capacity"
											+ " of the block sent: This: " + blockCapacity + " Sent: " + 
											newBlock.getCapacity());
		for(int i = 0; i < blockCapacity; i++)
			newBlock.setByte(i, block[blockNumber].getByte(i)); 
	}
	
	/**
	 * Returns a nonnegative integer value corresponding to the number of blocks 
	 * this disk holds at this instant.
	 * @return disk's capacity.
	 */
	public int getDiskCapacity() 
	{
		return diskCapacity;
	}
	
	/**
	 * Returns a nonnegative integer value corresponding to the size of one block 
	 * in this disk instance.
	 * @return disk's block size.
	 */
	public int getBlockCapacity()
	{
		return blockCapacity;
	}
	
	/**
	 * Turns on an existing disk unit.
	 * @param name the name of the disk unit to activate.
	 * @return the corresponding DiskUnit object.
	 * @throws NonExistingDiskException whenever no disk with the specified name 
	 * is found.
	 */
	public static DiskUnit mount(String name) throws NonExistingDiskException
	 {
		File file = new File(name);
		if (!file.exists())
			throw new NonExistingDiskException("No disk has name : " + name);
		
		DiskUnit dUnit = new DiskUnit(name);
		
		//Gets the capacity and the block size of the disk from the file
		//emulating the specified disk's storage area.
		
		try {
			dUnit.disk.seek(0);
			dUnit.diskCapacity = dUnit.disk.readInt();
			dUnit.blockCapacity = dUnit.disk.readInt();
			dUnit.disk.seek(0); //We reset so we can read all the data to the virtual blocks.
			dUnit.block = new DiskBlock[dUnit.diskCapacity];
			for (int i = 0; i < dUnit.diskCapacity; i++)
			{
				dUnit.block[i] = new DiskBlock(dUnit.blockCapacity);
				dUnit.block[i].readBlock(dUnit.disk);
			}
		   	} catch (IOException e) {
			   e.printStackTrace();
		   	}
		   	
		return dUnit;
	 }
	
	/**
	 * Reserves the needed space for this disk to operate by writing empty blocks to 
	 * the random access file. It also writes two integer values in the first 8 bytes 
	 * of the file (i.e. in block 0 of the disk): the disk capacity and block capacity
	 * of the disk being emulated, respectively.
	 * @param diskCapacity this disk's block capacity.
	 * @param blockCapacity this disk's blocks' bytes capacity.
	 */
	private static void reserveDiskSpace(RandomAccessFile disk, int diskCapacity, int blockCapacity) 
	{
		try {
				disk.setLength(diskCapacity * blockCapacity);
		    } catch (IOException e) {
		        e.printStackTrace();
		    }

		    //Write disk parameters (number of blocks, bytes per block) in
		    //disk's block 0.
		try {
		        disk.seek(0);
		        disk.writeInt(diskCapacity);  
		        disk.writeInt(blockCapacity);
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
	}
	
	/**
	 * Formats the disk by visiting every �physical block� in the disk and filling 
	 * them with zeros. Also, it randomly marks blocks as damaged with a probability 
	 * of 1e-5.
	 */
	public void lowLevelFormat()
	{
		for(DiskBlock b : block)
		{
			b.format();
			if (Math.random() < 0.00001)
				b.damage();
		}
	}
	
	/**
	 * Simulates the process of turning off a disk unit by closing the random access
	 * file that is emulating this disk storage area.
	 */
	public void shutdown() 
	{
		try {
			disk.close();
		  } catch (IOException e) {
			  e.printStackTrace();
		  }
	}
	
//*************************************MISCELLANEOUS METHODS*********************************************//	
	
	/**
	 * Tests if the parameter specified is a power of 2, that is, if base 2
	 * log(value) has no fractional value.
	 * @param value the value to be tested.
	 * @return true if the value is a power of 2, else returns false.
	 */
	private static boolean powerOf2(int value) 
	{
		return (Math.log10(value) / Math.log10(2)) == Math.floor(Math.log10(value) / Math.log10(2));
	}

}
